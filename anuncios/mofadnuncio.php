<?php
	include '../config.php';
	session_start();
	if (isset($_SESSION['adm'])) {
		$usuariounico=$_SESSION['adm'];
		$sacarinform="SELECT * from administrador where usuadmin='$usuariounico'";
		$queryad=mysql_query($sacarinform,$conexion) or die (mysql_error());
		while ($adv=mysql_fetch_array($queryad)) {
			$idad=$adv['id_admin'];
			$correo=$adv['correo_adm'];
			$avatar=$adv['avatar_adm'];
			$tpad=$adv['tip_adm'];
		}
		$idR=$_GET['an'];
		if ($idR=="") {
			echo "<script>";
				echo "alert('id anuncio no disponible');";
				echo "var pag='../erroadm.html';";
				echo "document.location.href=pag;";
			echo "</script>";
		}
		else{
			$datos="SELECT * from anuncio where id_an=$idR";
			$sql_datos=mysql_query($datos,$conexion) or die (mysql_error());
			$numdatos=mysql_num_rows($sql_datos);
			if ($numdatos>0) {
				while ($dt=mysql_fetch_array($sql_datos)) {
					$ttam=$dt['tt_an'];
					$rtan=$dt['rut_an'];
					$xxan=$dt['txx_an'];
					$fean=$dt['fe_an'];
					$esan=$dt['es_an'];
				}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1" />
	<link rel="icon" href="../imagenes/logo.png" />
	<title><?php echo "$ttam"; ?>|Seguimiento</title>
	<link rel="stylesheet" href="../css/normalize.css" />
	<link rel="stylesheet" href="../css/iconos/style.css" />
	<link rel="stylesheet" href="../css/style.css" />
	<link rel="stylesheet" href="../css/menu.css" />
	<script src="../js/jquery_2_1_1.js"></script>
	<script src="../js/scripamd.js"></script>
	<script src="../js/anuncios.js"></script>
</head>
<body>
	<header>
		<article>
			<nav id="mnP">
				<ul>
					<li>
						<a class="selecionado" href="../anuncios">Anuncios</a>
					</li>
					<?php
						if ($tpad=="1") {
					?>
					<li>
						<a href="../administrador">Seguimiento</a>
						<div class="submen" data-num="2"><span class="icon-ctrl"></span></div>
						<ul class="children2">
							<li><a href="../administrador/clientes">Clientes</a></li>
							<li><a href="../administrador/direcionador">Direcionador</a></li>
							<li><a href="../administrador/planes">Planes</a></li>
							<li><a href="../administrador/proveedores">Proveedores</a></li>
							<li><a href="../administrador/sectores">Sectores</a></li>
						</ul>
					</li>
					<li>
						<a href="../contabilidad">Contabilidad</a>
						<div class="submen" data-num="1"><span class="icon-ctrl"></span></div>
						<ul class="children1">
							<li><a href="../contabilidad/pendiente">Cuentas por Cobrar</a></li>
							<li><a href="../contabilidad/pagos">Cuentas por Pagar</a></li>
							<li><a href="../contabilidad/ingresos">Ingresos</a></li>
							<li><a href="../contabilidad/cajas">Caja</a></li>
						</ul>
					</li>
					<?php
						}
					?>
					<li>
						<a href="../tareas">Tareas</a>
						<div class="submen" data-num="3"><span class="icon-ctrl"></span></div>
						<ul class="children3">
							<li><a href="../tareas/mis_tareas">Mis tareas</a></li>
							<li><a href="../tareas/indicador">Indicadores</a></li>
							<li><a href="../tareas/indicadorTotal">Indicador total</a></li>
						</ul>
					</li>
					<?php
						if ($tpad=="1") {
					?>
					<li><a href="../comercial">Comercial</a></li>
					<?php
						}
					?>
					<li><a href="../clientes">Clientes</a></li>
					<li><a href="../eventos">Eventos</a></li>
					<li><a href="../cerrar">Salir</a></li>
				</ul>
			</nav>
			<div id="mn_mov"><span class="icon-menu"></span></div>
			<div id="ifmorver" class="veradmC">
				<?php 
					// echo "$usuariounico";
					$imagenadv="../".$avatar;
				?>
				<figure style="background-image:url(<?php echo $imagenadv ?>);">
					<!--Imagen-->
				</figure>
			</div>
		</article>
		<div class="botnomenu">
			<center><img src="../imagenes/abajo_b.png" alt="abajo" /></center>
		</div>
	</header><!-- /header -->
	<section class="margen">
		<nav id="menub">
			<?php
				if ($tpad=="1") {
			?>
			<a class="dislka" href="../anuncios">Ver anuncios</a>
			<?php
				}
			?>
		</nav>
		<h1><?php echo "$ttam"; ?></h1>
		<article>
			<h2>Imagen</h2>
			<form action="#" method="post" enctype="multipart/form-data" 
				id="fevan" class="columninput">
				<input type="text" id="idf" name="idf" value="<?php echo $idR ?>" required style="display:none;" />
				<a href="../<?php echo $rtan ?>" target="_blank"><?php echo "$rtan"; ?></a>
				<input type="file" id="fangg" name="fangg" required />
				<div id="txA"></div>
				<input type="submit" value="Modificar" id="ffcamimgan" class="botonstyle" />
			</form>
		</article>
		<article>
			<h2>Datos</h2>
			<form action="#" method="post" class="columninput">
			<label for="tta">*<b>Titulo</b></label>
			<input type="text" id="tta" name="tta" value="<?php echo $ttam ?>" required />
			<label for="xxa">Mensaje</label>
			<textarea name="xxa" id="xxa" rows="4"><?php echo "$xxan"; ?></textarea>
			<div id="txB"></div>
			<input type="submit" value="Modificar" id="mfdataun" data-id="<?php echo $idR ?>" class="botonstyle" />
			</form>
		</article>
	</section>
	<footer>
		<span id="idamd" data-id="<?php echo $idad ?>"></span>
	</footer>
</body>
</html>
<?php
			}
			else{
					echo "<script>";
						echo "alert('El anuncio no existe o ha sido eliminado');";
						echo "var pag='../erroadm.html';";
						echo "document.location.href=pag;";
					echo "</script>";
			}
		}
	}
	else{
		echo "<script>";
			echo "var pag='../erroadm.html';";
			echo "document.location.href=pag;";
		echo "</script>";
	}
?>