<?php
	include '../config.php';
	session_start();
	if (isset($_SESSION['adm'])) {
		$usuariounico=$_SESSION['adm'];
		$sacarinform="SELECT * from administrador where usuadmin='$usuariounico'";
		$queryad=mysql_query($sacarinform,$conexion) or die (mysql_error());
		while ($adv=mysql_fetch_array($queryad)) {
			$idad=$adv['id_admin'];
			$correo=$adv['correo_adm'];
			$avatar=$adv['avatar_adm'];
			$tpad=$adv['tip_adm'];
		}
		$dH=date("d");
		$dbH=date("j");
		$mH=date("m");
		$mbH=date("n");
		$yH=date("Y");
		$fein=$yH."-".$mH."-00";
		$feff=$yH."-".$mH."-31";
		$hoy=$yH."-".$mH."-".$dH;
		$nommeses=["Meses","Enero","Febrero","Marzo","Abril",
			"Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1" />
	<link rel="icon" href="../imagenes/icono.png" />
	<title>Anuncios|Seguimiento</title>
	<link rel="stylesheet" href="../css/normalize.css" />
	<link rel="stylesheet" href="../css/iconos/style.css" />
	<link rel="stylesheet" href="../css/style.css" />
	<link rel="stylesheet" href="../css/menu.css" />
	<script src="../js/jquery_2_1_1.js"></script>
	<script src="../js/scripamd.js"></script>
	<script src="../js/anuncios.js"></script>
</head>
<body>
	<header>
		<article>
			<nav id="mnP">
				<ul>
					<li>
						<a class="selecionado" href="../anuncios">Anuncios</a>
					</li>
					<?php
						if ($tpad=="1") {
					?>
					<li>
						<a href="../administrador">Seguimiento</a>
						<div class="submen" data-num="2"><span class="icon-ctrl"></span></div>
						<ul class="children2">
							<li><a href="../administrador/clientes">Clientes</a></li>
							<li><a href="../administrador/direcionador">Direcionador</a></li>
							<li><a href="../administrador/planes">Planes</a></li>
							<li><a href="../administrador/proveedores">Proveedores</a></li>
							<li><a href="../administrador/sectores">Sectores</a></li>
						</ul>
					</li>
					<li>
						<a href="../contabilidad">Contabilidad</a>
						<div class="submen" data-num="1"><span class="icon-ctrl"></span></div>
						<ul class="children1">
							<li><a href="../contabilidad/pendiente">Cuentas por Cobrar</a></li>
							<li><a href="../contabilidad/pagos">Cuentas por Pagar</a></li>
							<li><a href="../contabilidad/ingresos">Ingresos</a></li>
							<li><a href="../contabilidad/cajas">Caja</a></li>
						</ul>
					</li>
					<?php
						}
					?>
					<li>
						<a href="../tareas">Tareas</a>
						<div class="submen" data-num="3"><span class="icon-ctrl"></span></div>
						<ul class="children3">
							<li><a href="../tareas/mis_tareas">Mis tareas</a></li>
							<li><a href="../tareas/indicador">Indicadores</a></li>
							<li><a href="../tareas/indicadorTotal">Indicador total</a></li>
						</ul>
					</li>
					<?php
						if ($tpad=="1") {
					?>
					<li><a href="../comercial">Comercial</a></li>
					<?php
						}
					?>
					<li><a href="../clientes">Clientes</a></li>
					<li><a href="../eventos">Eventos</a></li>
					<li><a href="../cerrar">Salir</a></li>
				</ul>
			</nav>
			<div id="mn_mov"><span class="icon-menu"></span></div>
			<div id="ifmorver" class="veradmC">
				<?php 
					// echo "$usuariounico";
					$imagenadv="../".$avatar;
				?>
				<figure style="background-image:url(<?php echo $imagenadv ?>);">
					<!--Imagen-->
				</figure>
			</div>
		</article>
		<div class="botnomenu">
			<center><img src="../imagenes/abajo_b.png" alt="abajo" /></center>
		</div>
	</header><!-- /header -->
	<section class="margen">
		<nav id="menub">
			<?php
				if ($tpad=="1") {
			?>
			<div id="gbtn">Nuevo anuncio</div>
			<?php
				}
			?>
			<select id="mesBcb" data-id="<?php echo $idad ?>">
				<?php
					for ($mmb=0; $mmb <=12 ; $mmb++) {
						if ($mmb==$mbH) {
							$selmes="selected";
						}
						else{
							$selmes="";
						}
				?>
				<option value="<?php echo $mmb ?>" <?php echo $selmes ?>><?php echo $nommeses[$mmb]; ?></option>
				<?php
					}
				?>
			</select>
			<select id="yearBcb" data-id="<?php echo $idad ?>">
				<?php
					for ($yyB=2014; $yyB <=($yH+1) ; $yyB++) {
						if ($yyB==$yH) {
							$selyear="selected";
						}
						else{
							$selyear="";
						}
				?>
				<option value="<?php echo $yyB ?>" <?php echo $selyear ?>><?php echo "$yyB"; ?></option>
				<?php
					}
				?>
			</select>
		</nav>
		<article id="cjaG" class="uoculcjG">
			<form action="#" method="post" enctype="multipart/form-data" id="anev" class="columninput">
			<label for="tta">*<b>Titulo</b></label>
			<input type="text" id="tta" name="tta" required />
			<label for="gia">*<b>Imagen</b></label>
			<input type="file" id="gia" name="gia" required />
			<label for="xxa">Mensaje</label>
			<textarea name="xxa" id="xxa" rows="4"></textarea>
			<div id="txA"></div>
			<input type="submit" value="Ingresar" id="nvanun" class="botonstyle" />
			</form>
		</article>
		<h1>Anuncios</h1>
		<article id="anunT" class="colanunc"></article>
	</section>
	<footer>
		<span id="idamd" data-id="<?php echo $idad ?>"></span>
	</footer>
</body>
</html>
<?php
	}
	else{
		echo "<script>";
			echo "var pag='../erroadm.html';";
			echo "document.location.href=pag;";
		echo "</script>";
	}
?>