$(document).on('ready', inicio_anuncio);
function inicio_anuncio () {
	var idad=$("#idamd").attr("data-id");
	$("#nvanun").on("click",nuevo_anuncio);
	$("#ffcamimgan").on("click",cambiar_img_anuncio);
	$("#mfdataun").on("click",camb_dat_anuncio);
	$("#anunT").on("click",".deli",borrrgnr);
	$("#menub select").on("change",filtrar);
	$("#noext").remove();
	$(".colanunc").prepend(loadingBb);
	$.post("Tanun.php",{a:idad},colocartodos);
	console.log(idad);
}
var bien={color:"#005189"};
var normal={color:"#CD0000"};
var loading="<center><img src='../imagenes/loading.gif' alt='loading' style='width:10px;' /></center>";
var loadingBb="<center id='loadG'><img src='../imagenes/loading.gif' alt='loading' /></center>";
function writeanuncio (id,tit,img,text,fe,admin) {
	var html='<article id="caj'+id+'" class="ycj">';
		html+='<h2>'+tit+'</h2>';
		html+='<article class="flcj">';
			html+='<figure>';
				html+='<img src="../'+img+'" alt='+tit+' />'
			html+='</figure>';
			html+='<figcaption>';
				html+='<div class="text">';
					html+=text;
				html+='</div>';
				if (admin=="1") {
					html+='<a class="dislka" href="mofadnuncio.php?an='+id+'">Modificar</a>&nbsp;&nbsp;';
					html+='<a class="deli" href="borr_anuncio.php" data-id='+id+'>Borrar</a>';
				}
				html+='<div class="fech">'+fe+'</div>';
			html+='</figcaption>';
		html+='</article>';
	html+='</article>';
	return html;
}

function es_imagen (tipederf) {
	switch(tipederf.toLowerCase()){
		case 'jpg':
			return true;
			break;
		case 'gif':
			return true;
			break;
		case 'png':
			return true;
			break;
		case 'jpeg':
			return true;
			break;
		default:
			return false;
			break;
	}
}

function nuevo_anuncio () {
	var ttan = $("#tta").val();
	var igan = $("#gia")[0].files[0];
	var nameigan=igan.name;
	var exteigan=nameigan.substring(nameigan.lastIndexOf('.')+1);
	var tamigan=igan.size;
	var tipoigan=igan.type;
	if (ttan == "") {
		$("#txA").css(normal).text("Ingrese el titulo");
		return false;
	}
	else{
		if (!es_imagen(exteigan)) {
			$("#txA").css(normal).text("tipo de imagen no permitido");
			return false;
		}
		else{
			$("#txA").css(normal).text("");
			var formu=new FormData($("#anev")[0])
			$.ajax({
				url: "../new_anuncio.php",
				type: 'POST',
				data: formu,
				cache: false,
				contentType: false,
				processData: false,
				beforeSend:function () {
					$("#txA").prepend(loading)
				},
				success:relimgcont,
				error:function () {
					$("#txA").css(colhtm.colors("mal")).text("Ocurrió un error");
					$("#txA").fadeIn();$("#txA").fadeOut(3000);
				}
			})
			return false;
		}
	}
}

function relimgcont (resig) {
	if (resig=="2") {
		$("#txA").css(normal).text("Carpeta sin permisos o resolución de imagen no permitido");
		$("#txA").fadeIn();$("#txA").fadeOut(3000);
		return false;
	}
	else{
		if (resig=="3") {
			$("#txA").css(normal).text("Tamaño no permitido");
			$("#txA").fadeIn();$("#txA").fadeOut(3000);
			return false;
		}
		else{
			if (resig=="4") {
				$("#txA").css(normal).text("Carpeta sin permisos o resolución de imagen no permitido");
				$("#txA").fadeIn();$("#txA").fadeOut(3000);
				return false;
			}
			else{
				if (resig=="5") {
					$("#txA").css(bien).text("Ingresado");
					$("#txA").fadeIn();$("#txA").fadeOut(3000);
					location.reload(20);
				}
				else{
					$("#txA").css(normal).html(resig);
					$("#txA").fadeIn();
					return false;
				}
			}
		}
	}
}

function colocartodos (res) {
	$(".colanunc #loadG").remove()
	var rur = eval(res);
	console.log(rur)
	if (rur==1) {
		$('.colanunc #noext').remove();
		$(".colanunc").prepend("<span id='noext'>No hay anuncios</span>");
	}
	else{
		$("#noext").remove();
		for (var i = 0; i < rur.length; i++) {
			$(".colanunc").prepend(writeanuncio(rur[i].id,rur[i].tt,rur[i].rt,rur[i].xx,rur[i].fe,rur[i].ad))
				.fadeIn(5000)
		}
	}
}

function borrrgnr (e) {
	e.preventDefault();
	var ida=$(this).attr("data-id");
	var btacp=confirm("Estas seguro de elimarnlo?");
	if (btacp===true) {
		$.post("borr_anuncio.php",{a:ida},resultborrado(ida));
	}
}
function resultborrado (ida) {
	return function (res) {
		if (res=="2") {
			$("#caj"+ida).animate({width:"0",height:"0","overflow": "hidden"}, 500,quitarC(ida))
		}
		else{
			alert("Ocurrio un error");
			console.log(res)
		}
	}
}
function quitarC (id) {
	$("#caj"+id).css({"overflow": "hidden","margin":"0","padding":"0"})
}
function filtrar () {
	$(".colanunc .ycj").remove()
	$(".colanunc").prepend(loadingBb);
	var fa = $("#mesBcb").val()
	var fb = $("#yearBcb").val()
	var ad = $(this).attr("data-id")
	$.post("filanunc.php",{a:fa,b:fb,c:ad},colocartodos)
}
function cambiar_img_anuncio () {
	var caaa=$("#idf").val();
	var cabb=$("#fangg")[0].files[0];
	var namecabb=cabb.name;
	var extecabb=namecabb.substring(namecabb.lastIndexOf('.')+1);
	var tamcabb=cabb.size;
	var tipocabb=cabb.type;
	if (caaa=="" || caaa=="0") {
		$("#txA").css(normal).text("id de anuncio no disponible");
		return false;
	}
	else{
		if (!es_imagen(extecabb)) {
			$("#txA").css(normal).text("tipo de imagen no permitido");
			return false;
		}
		else{
			$("#txA").css(normal).text("");
			var formu=new FormData($("#fevan")[0])
			$.ajax({
				url: "../mof_anuncio.php",
				type: 'POST',
				data: formu,
				cache: false,
				contentType: false,
				processData: false,
				beforeSend:function () {
					$("#txA").prepend(loading)
				},
				success:relimgcont,
				error:function () {
					$("#txA").css(colhtm.colors("mal")).text("Ocurrió un error");
					$("#txA").fadeIn();$("#txA").fadeOut(3000);
				}
			})
			return false;
		}
	}
}
function camb_dat_anuncio () {
	var idb=$(this).attr("data-id");
	var aca=$("#tta").val();
	var acb=$("#xxa").val();
	if (aca=="") {
		$("#txB").css(normal).text("Ingrese el titulo");
		return false;
	}
	else{
		$("#txB").css(normal).text("");
		$("#txB").prepend(loading);
		$.post("mofdat_anuncio.php",{fa:idb,a:aca,b:acb},resuoff)
		return false;
	}
}
function resuoff (res) {
	if (res=="2") {
		$("#txB").css(normal).text("Modificado");
		location.reload(20);
	}
	else{
		$("#txB").css(normal).html(res);
		return false;
	}
}